﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameMaster : MonoBehaviour 
{	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetButtonDown("Reset"))
		{
			SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		}
	}
}
