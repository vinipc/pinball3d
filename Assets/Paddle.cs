﻿using UnityEngine;
using System.Collections;

public class Paddle : MonoBehaviour 
{
	public string buttonName;

	private HingeJoint hingeJoint;

	private void Awake()
	{
		hingeJoint = GetComponent<HingeJoint>();
	}

	private void Update()
	{
		hingeJoint.useMotor = Input.GetButton(buttonName);
	}
}